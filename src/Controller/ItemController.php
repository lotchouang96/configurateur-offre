<?php

namespace App\Controller;

use App\Entity\Item;
use App\Entity\Promotion;
use App\Form\ItemType;
use App\Repository\ItemRepository;
use App\Repository\PromotionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

#[Route('/item')]
class ItemController extends AbstractController
{
    /**
     * @param ItemRepository $itemRepository
     * @return Response
     * cette fonction renvoie la liste des articles
     */
    #[Route('/', name: 'item_index', methods: ['GET'])]
    public function index(ItemRepository $itemRepository): Response
    {
        return $this->render('item/index.html.twig', [
            'items' => $itemRepository->findAll(),
        ]);
    }

    /**
     * @param ItemRepository $itemRepository
     * @return Response
     * renvoi la liste des articles codifier en Json qui sera consommé par le CRM
     */
    #[Route('/json', name: 'item_json', methods: ['GET'])]
    public function itemJson(ItemRepository $itemRepository): Response
    {
        $data = [];
        foreach ($itemRepository->findAll() as $item)
        {
            $data[]['id'] = $item->getId();
            $data[]['name'] = $item->getName();
            $data[]['description'] = $item->getDescription();
            $data[]['quantity'] = $item->getQuantity();
            $data[]['price'] = $item->getPrice();
        }

        return new JsonResponse($data, Response::HTTP_OK);

    }

    /**
     * @param ItemRepository $itemRepository
     * @return Response
     * renvoi la liste des articles avec leurs promotions codifier en Json qui sera consommé par le CRM
     */
    #[Route('/json-promotion', name: 'item_json_promotion', methods: ['GET'])]
    public function itemJsonPromotion(ItemRepository $itemRepository): Response
    {
        $data = [];
        foreach ($itemRepository->findAll() as $item)
        {
            $data[]['id'] = $item->getId();
            $data[]['name'] = $item->getName();
            $data[]['description'] = $item->getDescription();
            $data[]['quantity'] = $item->getQuantity();
            $data[]['price'] = $item->getPrice();
            $data[]['price-promotion'] = $item->getPromotions()->first()->getPrice();
        }

        return new JsonResponse($data, Response::HTTP_OK);

    }

    /**
     * @param Request $request
     * @return Response
     * fonction permettant la création d'un nouvel article
     */
    #[Route('/new', name: 'item_new', methods: ['GET','POST'])]
    public function new(Request $request): Response
    {
        $item = new Item();
        $form = $this->createForm(ItemType::class, $item);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($item);
            $entityManager->flush();

            return $this->redirectToRoute('item_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('item/new.html.twig', [
            'item' => $item,
            'form' => $form,
        ]);
    }

    /**
     * @param Item $item
     * @return Response
     * Permet d'afficher les details de l'articles
     */
    #[Route('/{id}', name: 'item_show', methods: ['GET'])]
    public function show(Item $item): Response
    {
        return $this->render('item/show.html.twig', [
            'item' => $item,
        ]);
    }

    /**
     * @param Request $request
     * @param Item $item
     * @return Response
     * permet de editer l'article ainsi que ces options
     */
    #[Route('/{id}/edit', name: 'item_edit', methods: ['GET','POST'])]
    public function edit(Request $request, Item $item): Response
    {
        $form = $this->createForm(ItemType::class, $item);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('item_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('item/edit.html.twig', [
            'item' => $item,
            'form' => $form,
        ]);
    }

    /**
     * @param Request $request
     * @param Item $item
     * @return Response
     * permet la suppression d'un article
     */
    #[Route('/{id}', name: 'item_delete', methods: ['POST'])]
    public function delete(Request $request, Item $item): Response
    {
        if ($this->isCsrfTokenValid('delete'.$item->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($item);
            $entityManager->flush();
        }

        return $this->redirectToRoute('item_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * cette fonction permet de recupérer l'ensomble des promotion faitent sur les articles et les ajoutes ou mets à jour le prix
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function getPromotions(HttpClientInterface $client, ItemRepository $itemRepository, PromotionRepository $promotionRepository)
    {
        $em = $this->getDoctrine()->getManager();

        $request = $client->request('GET', 'http://localhost/CRM/promotionJSON.php' );
        $datas = $request->toArray();

        foreach ($datas as $data)
        {
            $item = $itemRepository->find($data['id']);
            /** @var Promotion $promotion */
            $promotion = $promotionRepository->findBy(['item' => $item->getId()]);
            if ($promotion === null)
            {
                $promotion = new Promotion();
                $promotion->setItem($item);
            }
            $promotion->setPrice($data['price']);
            $em->persist($promotion);
        }

        $em->flush();
    }


}
